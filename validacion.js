
document.addEventListener('DOMContentLoaded', function () {
    const formulario = document.querySelector('form');

    formulario.addEventListener('submit', function (event) {
        event.preventDefault();

        // Obtener los valores ingresados por el usuario
        const nombre = document.querySelector('input[name="name_control"]').value;
        const apellidos = document.querySelector('input[name="lastname_control"]').value;
        const email = document.querySelector('input[name="email_control"]').value;
        const fechaNacimiento = document.querySelector('input[name="date_control"]').value;
        const telefono = document.querySelector('input[name="tel_control"]').value;
        const genero = document.querySelector('select[name="genero"]').value;
        const estadoCivil = document.querySelector('select[name="estado_civil"]').value;
        const pais = document.querySelector('input[name="coun_control"]').value;
        const ciudad = document.querySelector('input[name="city_control"]').value;
        const direccion = document.querySelector('input[name="dir_control"]').value;
        const codigoPostal = document.querySelector('input[name="pos_control"]').value;

        // Crear un objeto para almacenar los datos
        const datosCliente = {
            nombre,
            apellidos,
            email,
            fechaNacimiento,
            telefono,
            genero,
            estadoCivil,
            pais,
            ciudad,
            direccion,
            codigoPostal
        };

        // Puedes realizar operaciones con los datos aquí
        console.log(datosCliente);

        // Limpia el formulario después de enviar los datos
        formulario.reset();
    });
});
